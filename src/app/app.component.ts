import { Component, ViewChild } from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {Project} from "../models/project";
import { AngularFirestore } from '@angular/fire/firestore';
import {ApiProvider} from "../providers/api/api";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'DashboardPage';

  allProjects: Project[] = [];
  active_id = "";

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,
              private firestore: AngularFirestore,
              private service: ApiProvider) {
    this.initializeApp();

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();

      this.service.getProjects().subscribe(actionArray => {
        this.allProjects = actionArray.map(item=>{
          return {
            id: item.payload.doc.id,
            ...item.payload.doc.data()
          } as Project
        });
        this.allProjects.sort(function(a, b){ return a.index-b.index});
        // when there is no active one.
        //console.log("active id : " + this.active_id);

        if(!this.active_id) {
          for(let project of this.allProjects){
            if(!project.is_sect){
              this.active_id = project.id;
              localStorage.setItem('projectid', this.active_id);
              return;
            }
          }
        }
      })

    });
  }

  openDashboard(projectid : string) {
    this.active_id = projectid;
    localStorage.setItem('projectid', this.active_id);
    this.nav.setRoot('DashboardPage');
  }
}
