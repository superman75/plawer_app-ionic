import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import * as firebase from 'firebase/app';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { ApiProvider } from '../providers/api/api';

const firebaseConfig = {
  apiKey: "AIzaSyD6Hb_KbQ9hX30NxXEE20kR3zEU_qmH0zU",
    authDomain: "plawer-a2bb5.firebaseapp.com",
    databaseURL: "https://plawer-a2bb5.firebaseio.com",
    projectId: "plawer-a2bb5",
    storageBucket: "plawer-a2bb5.appspot.com",
    messagingSenderId: "965804703896",
    appId: "1:965804703896:web:427ac12b93faf65e"
};
// Initialize firebase
firebase.initializeApp(firebaseConfig);

@NgModule({
  declarations: [
    MyApp
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider
  ]
})
export class AppModule {}
