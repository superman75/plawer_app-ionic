import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Task} from "../../models/task";
import {ApiProvider} from "../../providers/api/api";
import { AngularFirestore } from '@angular/fire/firestore';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  private projectId : string;
  private projectName : string;
  private projectDescription : string;
  allTasks: Task[] = [];
  newTask : string ;
  constructor(public navCtrl: NavController, public navParams: NavParams, private firestore: AngularFirestore,
              private service: ApiProvider) {
  }
  ionViewDidLoad() {
    this.projectId = localStorage.getItem('projectid');
    console.log(this.projectId);
    console.log('ionViewDidLoad DashboardPage');
    this.service.getProject(this.projectId).subscribe(actionArray => {
      actionArray.map(item=>{
        const data : any = item.payload.doc.data();
        //this.projectName = item.payload.doc.data().project_name;
        this.projectName = data.project_name;
        this.projectDescription = data.description;
      });
    });

    this.service.getTasks(this.projectId).subscribe(actionArray => {
      this.allTasks = actionArray.map(item=>{
        return {
          id: item.payload.doc.id,
          ...item.payload.doc.data()
        } as Task
      })
      this.allTasks.sort(function(a, b){ return a.index-b.index});
      console.log(this.allTasks);
    });


  }
  updateInterested(task: any){
    task.interested = !task.interested;
    this.firestore.doc("tasks/" + task.id).update(task);
  }

  updateStatus(task:any) {
    if(task.status=="progress"){
      task.status="completed"
    } else {
      task.status="progress"
    }
    this.firestore.doc("tasks/" + task.id).update(task);
  }


  archiveTask(task: any) {
    task.status = "completed";
    task.is_archived = true;
    this.firestore.doc("tasks/" + task.id).update(task);
  }
  deleteTask(task: any) {
    
  }
  eventHandler($event) {
    if($event.keyCode == 13) {

      if(!this.newTask){
        return;
      }
      let issect = false;
      if(this.newTask.charAt(this.newTask.length-1)==':'){
        this.newTask = this.newTask.slice(0, -1);
        issect = true;
      } else if(this.newTask.charAt(this.newTask.length-1)==' ' && this.newTask.charAt(this.newTask.length-2)==':'){
        this.newTask = this.newTask.slice(0, -2);
        issect = true;
      }

      for(let task of this.allTasks){
        task.index = task.index+1;
        this.firestore.doc('tasks/'+task.id).update(task);
      }

      this.firestore.collection("tasks").add(
        {
          task_name: this.newTask,
          is_sect: issect,
          status: 'progress',
          interested: false,
          disabled: false,
          index: 0,
          project_id: this.projectId,
          is_archived: false
        }
      );
      this.newTask = '';
    }
  }

}
