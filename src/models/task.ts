export class Task {
  id: string;
  is_sect: string;
  task_name: string;
  index: number;
  status: string;
  disabled: boolean;
  interested: boolean;
  project_id: string;
  is_archived: boolean;
}
