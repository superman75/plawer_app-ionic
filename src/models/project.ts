export class Project {
  id: string;
  image_url: string;
  is_sect: boolean;
  project_name: string;
  index: number;
  description: string;
}
