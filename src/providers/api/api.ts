import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

/*
  Generated class for the ApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {

  constructor(private firestore: AngularFirestore) {

  }
  getProjects() {
    return this.firestore.collection('projects').snapshotChanges();
  }

  getProject(projectId: string){
    return this.firestore.collection('projects', ref=> ref.where("id","==", projectId)).snapshotChanges();
  }

  getTasks(projectId: string){
    return this.firestore.collection('tasks', ref=> ref.where("project_id","==", projectId).where("is_archived", "==", false)).snapshotChanges();
  }
}
